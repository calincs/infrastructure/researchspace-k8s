# ResearchSpace K8s

A project for deploying ResearchSpace in Kubernetes.

See the Helm chart [README](charts/research-space/README.md) for installation and configuration instructions.

There is also an [rs-local.yaml](rs-local.yaml) manifest for easy installation of a test setup in a local cluster like Docker Desktop, K3s or Minikube.

The deployment is based off of the docker-compose project here: [https://github.com/researchspace/researchspace-docker-compose](https://github.com/researchspace/researchspace-docker-compose).
